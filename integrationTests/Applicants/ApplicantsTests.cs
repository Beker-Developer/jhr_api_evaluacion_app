﻿using integrationTests.Applicants.Fixture;
using jhr_api_evaluacion_app.Controllers;
using library.ControllerApplicants.RestModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace integrationTests.Applicants
{
  public partial class ApplicantsTests : IClassFixture<ApplicantsFixture>
  {
    #region Properties
    private readonly ApplicantsFixture _applicantsFixture;
    private readonly ApplicantsController _applicantsController;
    #endregion Properties

    #region Constructor
    public ApplicantsTests(ApplicantsFixture applicantsFixture) {
      _applicantsFixture = applicantsFixture;
      _applicantsController = new ApplicantsController(Options.Create(_applicantsFixture._appSettings), _applicantsFixture._mapper, _applicantsFixture._msSQLDB);
    }
    #endregion Constructor

    #region GET
    [Fact]
    public async Task GetByIdApplicantExists_True()
    {
      await _applicantsFixture.InsertApplicant(_applicantsFixture._dbmApplicants);
      ActionResult actionResult = await _applicantsController.Get(_applicantsFixture.id);
      await _applicantsFixture.ResetDataByAplicationIdAsync(_applicantsFixture.id);
      Assert.IsType<OkObjectResult>(actionResult);
      RMApplicant applicant = (RMApplicant?)((OkObjectResult)actionResult).Value?? new RMApplicant();
      Assert.Equal(_applicantsFixture.id, applicant.id);

    }

    [Fact]
    public async Task GetByIdApplicantExists_False()
    {
     
      ActionResult actionResult = await _applicantsController.Get(_applicantsFixture.idError);
      Assert.IsType<NotFoundResult>(actionResult);
    }
    #endregion GET

    #region POST
    [Fact]
    public async Task CreateApplicantExists_True()
    {
      ActionResult actionResult = await _applicantsController.Post(_applicantsFixture._rmBodyApplicantSuccess);
      Assert.IsType<OkObjectResult>(actionResult);
      RMApplicant applicant = (RMApplicant?)((OkObjectResult)actionResult).Value ?? new RMApplicant();
      await _applicantsFixture.ResetDataByAplicationIdAsync(applicant.id);
      Assert.IsType<OkObjectResult>(actionResult);
    }

    [Fact]
    public async Task CreateApplicantExists_False()
    {
      ActionResult actionResult = await _applicantsController.Post(_applicantsFixture._rmBodyApplicantError);
      Assert.IsType<BadRequestObjectResult>(actionResult);
    }
    #endregion POST
  }
}
