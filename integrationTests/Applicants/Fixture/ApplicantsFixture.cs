﻿using AutoMapper;
using library.DataBase.Applicants;
using library.General;
using Microsoft.Extensions.Configuration;
using MSSQL;
using Microsoft.Extensions.DependencyInjection;
using library.DataBase.Applicants.Models;
using library.ControllerApplicants.RestModel;
using library.General.Enums;

namespace integrationTests.Applicants.Fixture
{
  public class ApplicantsFixture//: IAsyncLifetime
  {
    #region Properties Configure
    public readonly AppSettings _appSettings;
    public readonly IMapper _mapper;
    public readonly MSSQLDB _msSQLDB;
    private readonly DBCApplicants _dbcApplicants;
    #endregion  Properties Configure

    #region Properties Tests
    public DBMApplicants _dbmApplicants;
    public RMBodyApplication _rmBodyApplicantSuccess;
    public RMBodyApplication _rmBodyApplicantError;
    public long? id;
    public long? idError;
    #endregion Properties Tests



    public ApplicantsFixture()
    {

      #region AppSettings
      IConfiguration configuration = new ConfigurationBuilder()
      .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
      .AddEnvironmentVariables()
      .Build();

      _appSettings = new AppSettings();
      configuration.Bind(_appSettings);
      #endregion AppSettings

      #region Mapper
      var mapperConfiguration = new MapperConfiguration(cfg =>
      {
        cfg.AddProfile(new MapperConfigurationProfile());
      });
      _mapper = mapperConfiguration.CreateMapper();
      #endregion

      #region DataBase
      _msSQLDB = new MSSQLDB(_appSettings.SQL_CONNECTION_STRING);
      _dbcApplicants = new DBCApplicants(_msSQLDB);
      #endregion  DataBase

      #region Objects
      idError = -1;
      _dbmApplicants = new DBMApplicants()
      {
        firstName = "Integrations",
        lastName = "Test",
        identifier = "Identif1er",
        age = 40,
        grimorioId = (int)Grimorio.BuenaFortuna,
        statusId = (int)Status.Aceptado,
        magicalAffinityId = (int)MagicalAffinity.Luz,
        createDate = DateTime.Now

      };

      _rmBodyApplicantSuccess = new RMBodyApplication()
      {
        firstName = "Juan",
        lastName = "Hernandez",
        identifier = "123XA",
        age = 31,
        magicalAffinity = MagicalAffinity.Tierra.ToString(),
      };

      _rmBodyApplicantError = new RMBodyApplication()
      {
        firstName = "Juan.12345678912345678",
        lastName = "Hernandez.12345678912345678?",
        identifier = "123XA?*ASDFGHJ",
        age = 1,
        magicalAffinity = "No exists",
      };
      #endregion Objects
    }

    #region Data
    internal async Task<DBMApplicants> InsertApplicant(DBMApplicants _dbmApplicants)
    {
      _dbmApplicants = await _dbcApplicants.InsertApplicantsAsync(_dbmApplicants);
      this.id = _dbmApplicants.id;
      return _dbmApplicants;
    }

    public async Task ResetDataByAplicationIdAsync(long? id)
    {
      await _dbcApplicants.DeleteApplicantsAsync(id);
    }
    #endregion Data

    #region Dispose
    //public async Task InitializeAsync()
    //{
    //  this._dbmApplicants = await InsertApplicant(this._dbmApplicants);
    //}

    //public async Task DisposeAsync()
    //{
    //  await ResetDataByAplicationIdAsync(this.id);
    //}

    #endregion Dispose
  }
}
