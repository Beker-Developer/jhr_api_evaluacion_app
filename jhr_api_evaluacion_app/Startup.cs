using AutoMapper;
using library.General;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using MSSQL;
using System;
using System.IO;
using System.Net;
using System.Reflection;

namespace jhr_api_evaluacion_app
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = new ConfigurationBuilder()
      //.SetBasePath(env.ContentRootPath)
      .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
      .AddEnvironmentVariables()
      .Build();
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddControllers();

      services.AddOptions(); // Servicio utilizado para leer los settings
      services.Configure<AppSettings>(Configuration); // Configuración para mapear los settigs a la clase AppSettings

      System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

      // Configuracion de base de datos SQL, Transient significa que se genera una nueva instancia por petición
      services.AddTransient<IMSSQLDB>(s => new MSSQLDB(Configuration.GetValue<string>("SQL_CONNECTION_STRING")));

      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
          Title = "IA Interactive  API",
          Version = "v1",
          Description = "Examen de ingreso",
          TermsOfService = new Uri("https://www.ia.com.mx/aviso-de-privacidad"),
          Contact = new OpenApiContact
          {
            Name = "Juan Antonio Hernandez Rosales",
            Email = "juan_hdez_rosales@hotmail.com",
            Url = new Uri("https://documenter.getpostman.com/view/3417939/2s93CPrYR1")
          },
        });

        var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
      });

      // Cofiguración de mapper, Singleton significa que solamente existir una sola instancia para toda la aplicación
      var config = new MapperConfiguration(cfg =>
      {
        cfg.AddProfile(new MapperConfigurationProfile());
      });
      IMapper mapper = config.CreateMapper();
      services.AddSingleton(mapper);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseRouting();

      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });

      app.UseSwagger();

      // This middleware serves the Swagger documentation UI
      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Employee API V1");
      });
    }
  }
}
