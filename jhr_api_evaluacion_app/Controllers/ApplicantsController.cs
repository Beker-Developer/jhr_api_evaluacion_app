﻿using AutoMapper;
using library.ControllerApplicants;
using library.ControllerApplicants.RestModel;
using library.General;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MSSQL;
using System.Threading.Tasks;

namespace jhr_api_evaluacion_app.Controllers
{
  [Route("api/applicants")]
  [ApiController]
  public class ApplicantsController : ControllerBase
  {
    #region
    private readonly IOptions<AppSettings> _appSettings;
    private readonly IMapper _mapper;
    private readonly IMSSQLDB _msSQLDB;
    #endregion

    public ApplicantsController(IOptions<AppSettings> appSettings, IMapper mapper, IMSSQLDB msSQLDB) {
      _appSettings = appSettings;
      _mapper = mapper;
      _msSQLDB = msSQLDB;
    }

    /// <summary>
    /// Get list of applicants.
    /// </summary>
    /// <param name="identifier"></param>
    /// <param name="grimorio" type="Grimorio">Filter by assigned griome, values: Sinceridad, Esperanza, Amor, Buena, Fortuna o Desesperacion</param>
    /// <response code="200">Successful query</response>
    [HttpGet("")]
    public async Task<ActionResult> Get(string identifier, string grimorio = null) 
      => await new MCApplicants(_appSettings, _mapper, _msSQLDB).Get(null, identifier, grimorio);

    /// <summary>
    /// Get applicant by id.
    /// </summary>
    /// <param name="id">Id applicant</param>
    /// <response code="200">Successful query</response>
    [HttpGet("{id}")]
    public async Task<ActionResult> Get(long? id)
     => await new MCApplicants(_appSettings, _mapper, _msSQLDB).Get(id, null, null);

    /// <summary>
    /// Register new applicant.
    /// </summary>
    /// <param name="applicants"></param>
    /// <returns>New registered applicant</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /applicants
    ///     {
    ///        "firstName": "Juan",
    ///        "lastName": "Hernandez",
    ///        "identifier": "41W45aAA11",
    ///        "age": 32,
    ///        "magicalAffinity": "Viento"
    ///     }
    ///
    /// </remarks>
    /// <response code="201">Returns the newly created item</response>
    /// <response code="404">An error occurred, check the list of errors</response>
    [HttpPost("")]
    public async Task<ActionResult> Post([FromBody] RMBodyApplication applicants)
    => await new MCApplicants(_appSettings, _mapper, _msSQLDB)
    .Post(applicants);


    /// <summary>
    /// Update applicant record.
    /// </summary>
    /// <param name="applicants"></param>
    /// <returns> Update Applicant Record</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /applicants
    ///     {
    ///        "firstName": "Juan",
    ///        "lastName": "Hernandez",
    ///        "age": 32,
    ///        "magicalAffinity": "Viento"
    ///     }
    ///
    /// </remarks>
    /// <param name="id">Id applicant</param>
    /// <response code="200">The registry update happened successfully.</response>
    /// <response code="404">An error occurred, check the list of errors</response>
    [HttpPut("{id}")]
    public async Task<ActionResult> Put(long? id, [FromBody] RMBodyApplication applicants)
   => await new MCApplicants(_appSettings, _mapper, _msSQLDB)
   .Put(id, applicants);

    /// <summary>
    /// Update applicant status. the possible values ​​of the status are: Pendiente, Revisión,Aceptado o Rechazado
    /// </summary>
    /// <param name="status"></param>
    /// <returns> Update Applicant Status</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /applicants
    ///     {
    ///        "status": "Aceptado"
    ///      
    ///     }
    ///
    /// </remarks>
    /// <param name="id">Id applicant</param>
    /// <response code="200">The registry update happened successfully.</response>
    /// <response code="404">An error occurred, check the list of errors</response>
    [HttpPut("{id}/status")]
    public async Task<ActionResult> PutStatus(long? id, [FromBody] RMApplicantStatus rmStatus)
  => await new MCApplicants(_appSettings, _mapper, _msSQLDB)
  .PutStatus(id, rmStatus);

    /// <summary>
    /// Delete applicant.
    /// </summary>
    /// <param name="id"></param>
    /// <response code="404">The applicant does not exist.</response>
    /// <response code="204">The applicant was successfully removed.</response>
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(long? id)
  => await new MCApplicants(_appSettings, _mapper, _msSQLDB)
  .Delete(id);

  }
}
