﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace MSSQL
{
  public interface IMSSQLDB
  {
    Task<int> ExecuteNonQueryAsync(string procedureName, List<SqlParameter> parameters, CommandType commandType = CommandType.StoredProcedure);
    //object ExecuteScalar(string procedureName, List<SqlParameter> parameters);
    Task<object> ExecuteScalar(string procedureName, List<SqlParameter> parameters);
    DataTable GetData(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure);
    Task<DataTable> GetData(string procedureName, List<SqlParameter> parameters, CommandType commandType = CommandType.StoredProcedure);
  }
}
