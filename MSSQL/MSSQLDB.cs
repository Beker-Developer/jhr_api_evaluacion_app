﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace MSSQL
{
  public class MSSQLDB: IMSSQLDB
  {
    protected string ConnectionString { get; set; }

    public MSSQLDB()
    {
    }

    public MSSQLDB(string connectionString)
    {
      this.ConnectionString = connectionString;
    }

    private SqlConnection GetConnection()
    {
      SqlConnection connection = new SqlConnection(this.ConnectionString);
      if (connection.State != ConnectionState.Open)
        connection.Open();
      return connection;
    }

    public DbCommand GetCommand(DbConnection connection, string commandText, CommandType commandType)
    {
      SqlCommand command = new SqlCommand(commandText, connection as SqlConnection);
      command.CommandType = commandType;
      return command;
    }

    public async Task<int> ExecuteNonQueryAsync(string procedureName, List<SqlParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
    {
      int returnValue = -1;

      try
      {
        using (SqlConnection connection = this.GetConnection())
        {
          DbCommand cmd = this.GetCommand(connection, procedureName, commandType);

          if (parameters != null && parameters.Count > 0)
          {
            cmd.Parameters.AddRange(parameters.ToArray());
          }

          returnValue = await cmd.ExecuteNonQueryAsync();
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }

      return returnValue;
    }

    public async Task<object> ExecuteScalar(string procedureName, List<SqlParameter> parameters)
    {
      object returnValue = null;

      try
      {
        using (DbConnection connection = this.GetConnection())
        {
          DbCommand cmd = this.GetCommand(connection, procedureName, CommandType.StoredProcedure);

          if (parameters != null && parameters.Count > 0)
          {
            cmd.Parameters.AddRange(parameters.ToArray());
          }

          returnValue = await cmd.ExecuteScalarAsync();
        }
      }
      catch (Exception ex)
      {
        //LogException("Failed to ExecuteScalar for " + procedureName, ex, parameters);
        throw ex;
      }

      return returnValue;
    }

    public DataTable GetData(string procedureName, List<DbParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
    {
      DbDataReader ds;
      DataTable dt = new DataTable();

      try
      {
        DbConnection connection = this.GetConnection();
        {
          DbCommand cmd = this.GetCommand(connection, procedureName, commandType);
          if (parameters != null && parameters.Count > 0)
          {
            cmd.Parameters.AddRange(parameters.ToArray());
          }

          ds = cmd.ExecuteReader(CommandBehavior.CloseConnection);
          dt.Load(ds);
        }
      }
      catch (Exception ex)
      {
        //LogException("Failed to GetDataReader for " + procedureName, ex, parameters);
        throw ex;
      }

      return dt;
    }

    public async Task<DataTable> GetData(string procedureName, List<SqlParameter> parameters, CommandType commandType = CommandType.StoredProcedure)
    {
      DbDataReader ds;
      DataTable dt = new DataTable();

      try
      {
        DbConnection connection = this.GetConnection();
        {
          DbCommand cmd = this.GetCommand(connection, procedureName, commandType);
          if (parameters != null && parameters.Count > 0)
          {
            cmd.Parameters.AddRange(parameters.ToArray());
          }

          ds = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);
          dt.Load(ds);
        }
      }
      catch (Exception ex)
      {
        //LogException("Failed to GetDataReader for " + procedureName, ex, parameters);
        throw ex;
      }

      return dt;
    }
  }
}
