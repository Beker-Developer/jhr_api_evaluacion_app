CREATE DATABASE [IA_Interactive]
GO
USE [IA_Interactive]
GO
/****** Object:  Table [dbo].[Applicants]    Script Date: 25/02/2023 05:53:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Applicants](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[firstName] [varchar](20) NULL,
	[lastName] [varchar](20) NULL,
	[identifier] [varchar](10) NULL,
	[age] [int] NULL,
	[grimorioId] [int] NULL,
	[statusId] [int] NULL,
	[magicalAffinityId] [int] NULL,
	[createDate] [datetime] NOT NULL,
	[acceptanceDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Grimorio]    Script Date: 25/02/2023 05:53:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grimorio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MagicalAffinity]    Script Date: 25/02/2023 05:53:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MagicalAffinity](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 25/02/2023 05:53:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Grimorio] ON 

INSERT [dbo].[Grimorio] ([id], [description]) VALUES (1, N'Sinceridad')
INSERT [dbo].[Grimorio] ([id], [description]) VALUES (2, N'Esperanza')
INSERT [dbo].[Grimorio] ([id], [description]) VALUES (3, N'Amor')
INSERT [dbo].[Grimorio] ([id], [description]) VALUES (4, N'Buena Fortuna')
INSERT [dbo].[Grimorio] ([id], [description]) VALUES (5, N'Desesperacion')
SET IDENTITY_INSERT [dbo].[Grimorio] OFF
GO
SET IDENTITY_INSERT [dbo].[MagicalAffinity] ON 

INSERT [dbo].[MagicalAffinity] ([id], [description]) VALUES (1, N'Oscuridad')
INSERT [dbo].[MagicalAffinity] ([id], [description]) VALUES (2, N'Luz')
INSERT [dbo].[MagicalAffinity] ([id], [description]) VALUES (3, N'Fuego')
INSERT [dbo].[MagicalAffinity] ([id], [description]) VALUES (4, N'Agua')
INSERT [dbo].[MagicalAffinity] ([id], [description]) VALUES (5, N'Viento')
INSERT [dbo].[MagicalAffinity] ([id], [description]) VALUES (6, N'Tierra')
SET IDENTITY_INSERT [dbo].[MagicalAffinity] OFF
GO
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([id], [description]) VALUES (1, N'Pendiente')
INSERT [dbo].[Status] ([id], [description]) VALUES (2, N'Revisión')
INSERT [dbo].[Status] ([id], [description]) VALUES (3, N'Aceptado')
INSERT [dbo].[Status] ([id], [description]) VALUES (4, N'Rechazado')
SET IDENTITY_INSERT [dbo].[Status] OFF
GO
ALTER TABLE [dbo].[Applicants]  WITH CHECK ADD FOREIGN KEY([grimorioId])
REFERENCES [dbo].[Grimorio] ([id])
GO
ALTER TABLE [dbo].[Applicants]  WITH CHECK ADD FOREIGN KEY([magicalAffinityId])
REFERENCES [dbo].[MagicalAffinity] ([id])
GO
ALTER TABLE [dbo].[Applicants]  WITH CHECK ADD FOREIGN KEY([statusId])
REFERENCES [dbo].[Status] ([id])
GO
/****** Object:  StoredProcedure [dbo].[sp_Applicants]    Script Date: 25/02/2023 05:53:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Applicants]
@Action int = null,
@id bigint = null,
@firstName varchar(20) = null,
@lastName varchar(20) = null,
@identifier varchar(20) = null,
@age int = null,
@grimorioId int = null,
@statusId int = null,
@magicalAffinityId int = null,
@createDate datetime = null,
@acceptanceDate datetime = null
AS
Declare @query varchar(max), @Mensaje_Error varchar(250)
IF @Action = 1 BEGIN
	SELECT @query = ''
	SELECT @query += char(10) + ' SELECT A.*, G.[description] grimorio, MA.[description] magicalAffinity,  S.[description] status '
	SELECT @query += char(10) + ' FROM Applicants A '
	SELECT @query += char(10) + ' LEFT JOIN Grimorio G ON G.id = A.grimorioId '
	SELECT @query += char(10) + ' LEFT JOIN MagicalAffinity MA ON MA.id = A.magicalAffinityId '
	SELECT @query += char(10) + ' LEFT JOIN Status S ON S.id = A.statusId '
	SELECT @query += char(10) + ' WHERE 1 = 1 '
	IF ISNULL(@id,0) != 0 BEGIN
		SELECT @query += char(10) + ' AND A.id = ' + LTRIM(RTRIM(STR(@id))) 
	END
	IF ISNULL(@identifier,'') != '' BEGIN
		SELECT @query += char(10) + ' AND A.identifier = ''' + @identifier + ''''
	END
	IF ISNULL(@grimorioId,0) != 0 BEGIN
		SELECT @query += char(10) + ' AND A.grimorioId = ' + LTRIM(RTRIM(STR(@grimorioId))) 
	END
	IF ISNULL(@statusId,0) != 0 BEGIN
		SELECT @query += char(10) + ' AND A.statusId = ' + LTRIM(RTRIM(STR(@statusId))) 
	END
	IF ISNULL(@magicalAffinityId,0) != 0 BEGIN
		SELECT @query += char(10) + ' AND A.magicalAffinityId = ' + LTRIM(RTRIM(STR(@magicalAffinityId))) 
	END
	PRINT @query
	exec(@query)
END ELSE IF @Action = 2 BEGIN
	BEGIN TRY 
		BEGIN TRAN 
			INSERT INTO Applicants (firstName, lastName, identifier, age, grimorioId, statusId, magicalAffinityId, createDate, acceptanceDate) 
			SELECT	@firstName,	@lastName, @identifier, @age, @grimorioId, @statusId, @magicalAffinityId, @createDate, @acceptanceDate
			SELECT @@IDENTITY AS id;  
			COMMIT TRAN 
	END TRY 
	BEGIN CATCH 
		SELECT @Mensaje_Error = ERROR_MESSAGE() 
		ROLLBACK TRAN 
		RAISERROR(@Mensaje_Error,19,1) WITH LOG
	END CATCH 
END ELSE IF @Action = 3 BEGIN
	BEGIN TRY 
		BEGIN TRAN 
			UPDATE Applicants SET 
				firstName = @firstName,
				lastName = @lastName,
				identifier = @identifier,
				age = @age,
				grimorioId = @grimorioId,
				statusId = @statusId,
				magicalAffinityId = @magicalAffinityId,
				createDate = @createDate,
				acceptanceDate = @acceptanceDate
			WHERE id = @id
			SELECT id = @id
			COMMIT TRAN 
	END TRY 
	BEGIN CATCH 
		SELECT @Mensaje_Error = ERROR_MESSAGE() 
		ROLLBACK TRAN 
		RAISERROR(@Mensaje_Error,19,1) WITH LOG
	END CATCH 
END ELSE IF @Action = 4 BEGIN
	BEGIN TRY 
		BEGIN TRAN 
			DELETE FROM Applicants	WHERE id = @id
			COMMIT TRAN 
		END TRY 
		BEGIN CATCH 
			SELECT @Mensaje_Error = ERROR_MESSAGE() 
			ROLLBACK TRAN 
			RAISERROR(@Mensaje_Error,19,1) WITH LOG
		END CATCH 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Grimorio]    Script Date: 25/02/2023 05:53:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Grimorio]
@Accion int = null,
@id int = null,
@description varchar(20) = null
AS
Declare @query varchar(max), @Mensaje_Error varchar(250)
IF @Accion = 1 BEGIN
	SELECT @query = ''
	SELECT @query += char(10) + ' SELECT A.* '
	SELECT @query += char(10) + ' FROM Grimorio A '
	SELECT @query += char(10) + ' WHERE 1 = 1 '
	IF ISNULL(@id,0) > 0 BEGIN
		SELECT @query += char(10) + ' AND A.id = ' + LTRIM(RTRIM(STR(@id))) 
	END
	exec(@query)
END ELSE IF @Accion = 2 BEGIN
	BEGIN TRY 
		BEGIN TRAN 
			INSERT INTO Grimorio ([description]) 
			SELECT @description
			COMMIT TRAN 
	END TRY 
	BEGIN CATCH 
		SELECT @Mensaje_Error = ERROR_MESSAGE() 
		ROLLBACK TRAN 
		RAISERROR(@Mensaje_Error,19,1) 
	END CATCH 
END ELSE IF @Accion = 3 BEGIN
	BEGIN TRY 
		BEGIN TRAN 
			UPDATE Grimorio SET 
				[description] = @description
			WHERE id = @id
			SELECT id = @id
			COMMIT TRAN 
	END TRY 
	BEGIN CATCH 
		SELECT @Mensaje_Error = ERROR_MESSAGE() 
		ROLLBACK TRAN 
		RAISERROR(@Mensaje_Error,19,1) 
	END CATCH 
END ELSE IF @Accion = 4 BEGIN
	BEGIN TRY 
		BEGIN TRAN 
			DELETE FROM Grimorio WHERE id = @id
			COMMIT TRAN 
		END TRY 
		BEGIN CATCH 
			SELECT @Mensaje_Error = ERROR_MESSAGE() 
			ROLLBACK TRAN 
			RAISERROR(@Mensaje_Error,19,1) 
		END CATCH 
END 
GO
/****** Object:  StoredProcedure [dbo].[sp_Status]    Script Date: 25/02/2023 05:53:35 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Status]
@Accion int = null,
@id int = null,
@description varchar(20) = ''
AS
Declare @query varchar(max), @Mensaje_Error varchar(250)
IF @Accion = 1 BEGIN
	SELECT @query = ''
	SELECT @query += char(10) + ' SELECT A.* '
	SELECT @query += char(10) + ' FROM  Status A '
	SELECT @query += char(10) + ' WHERE 1 = 1 '
	exec(@query)
END ELSE IF @Accion = 2 BEGIN
	BEGIN TRY 
		BEGIN TRAN 
			INSERT INTO [Status] ([description]) 
			SELECT 	@description
			COMMIT TRAN 
	END TRY 
	BEGIN CATCH 
		SELECT @Mensaje_Error = ERROR_MESSAGE() 
		ROLLBACK TRAN 
		RAISERROR(@Mensaje_Error,19,1) 
	END CATCH 
END ELSE IF @Accion = 3 BEGIN
	BEGIN TRY 
		BEGIN TRAN 
			UPDATE [Status] SET
				[description] = @description
			WHERE id = @id
			SELECT id = @id
			COMMIT TRAN 
	END TRY 
	BEGIN CATCH 
		SELECT @Mensaje_Error = ERROR_MESSAGE() 
		ROLLBACK TRAN 
		RAISERROR(@Mensaje_Error,19,1) 
	END CATCH 
END ELSE IF @Accion = 4 BEGIN
	BEGIN TRY 
		BEGIN TRAN 
			DELETE FROM [Status] WHERE id = @id
			COMMIT TRAN 
		END TRY 
		BEGIN CATCH 
			SELECT @Mensaje_Error = ERROR_MESSAGE() 
			ROLLBACK TRAN 
			RAISERROR(@Mensaje_Error,19,1) 
		END CATCH 
END 
GO
