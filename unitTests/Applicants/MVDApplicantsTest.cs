﻿using library.ControllerApplicants.Validations;
using library.General;
using unitTests.Applicants.Fixtures;
using Xunit;

namespace unitTests.Applicants
{


  public class MVDApplicantsTest : IClassFixture<ApplicantsFixture>
  {
      private readonly MVDApplicants _mvdApplicants;
      private readonly Validators _validators;
      private readonly ApplicantsFixture _applicantsFixture;

    public MVDApplicantsTest(ApplicantsFixture applicantsFixture)
    {
      _applicantsFixture = applicantsFixture;
      _validators = new Validators();
      _mvdApplicants = new MVDApplicants(applicantsFixture.IDBApplicantsMock.Object, _validators);
    }

    [Fact]
    public async Task ValidateGetDBMApplicants_Valid()
    {
      var result = await _mvdApplicants.ValidateExistsApplicantsAsync(_applicantsFixture.idSuccess);
      Assert.NotNull(result);
    }

    [Fact]
    public async Task ValidateGetDBMApplicants_InValid()
    {
      var result = await _mvdApplicants.ValidateExistsApplicantsAsync(_applicantsFixture.idError);
      Assert.Null(result);
    }
  }
}
