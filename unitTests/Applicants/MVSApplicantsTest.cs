﻿using library.ControllerApplicants.Validations;
using library.General;
using unitTests.Applicants.Fixtures;
using Xunit;

namespace unitTests.Applicants
{
  public class MVSApplicantsTest : IClassFixture<ApplicantsFixture>
  {
    private readonly MVSApplicants _mvsApplicants;
    private readonly Validators _validators;
    private readonly ApplicantsFixture _applicantsFixture;

    public MVSApplicantsTest(ApplicantsFixture applicantsFixture) {
      _applicantsFixture = applicantsFixture;
      _validators = new Validators();
      _mvsApplicants = new MVSApplicants(_validators);
    }

    [Fact]
    public void ValidateApplicants_Sucess()
    {
      var result = _mvsApplicants.ValidateApplicants(null, _applicantsFixture.rmBodyApplicationsSuccess, true, null);
      Assert.NotNull(result);
    }

    [Fact]
    public void ValidateApplicants_Error()
    {
      var result = _mvsApplicants.ValidateApplicants(null, _applicantsFixture.rmBodyApplicationsError, true, null);
      string errorsMessages = string.Join("-", _validators.errors.Select(x => x.error).ToList());
      Assert.Contains(MSError.Name_Exceed_Character_Limit.error, errorsMessages);
      Assert.Contains(MSError.Name_Contains_Only_Letter.error, errorsMessages);
      Assert.Contains(MSError.Last_Name_Exceed_Character_Limit.error, errorsMessages);
      Assert.Contains(MSError.Last_Name_Contains_Only_Letter.error, errorsMessages);
      Assert.Contains(MSError.Identifier_Exceed_Character_Limit.error, errorsMessages);
      Assert.Contains(MSError.Identifier_Contains_Only_Letter_And_Numbers.error, errorsMessages);
      Assert.Contains(MSError.Age_Exceed_Character_Limit.error, errorsMessages);
      Assert.Contains(MSError.No_Exists_Magical_Affinity.error, errorsMessages);

      Assert.Null(result);
    }

    [Fact]
    public void ValidatStatus_Sucess()
    {
      var result = _mvsApplicants.ValidateStatus("Aceptado");
      Assert.NotNull(result);
    }

    [Fact]
    public void ValidateStatus_Error()
    {
      var result = _mvsApplicants.ValidateStatus("NoExists");
      string errorsMessages = string.Join("-", _validators.errors.Select(x => x.error).ToList());
      Assert.Contains(MSError.No_Exists_Status.error, errorsMessages);
      Assert.Null(result);
    }

    [Fact]
    public void ValidatChangeStatusToAceptar_Sucess()
    {
      var result = _mvsApplicants.ValidateStatusChangeToAccepted(_applicantsFixture.dbmApplicantSuccess);
      Assert.NotNull(result);
    }

    [Fact]
    public void ValidatChangeStatusToAceptar_Error()
    {
      var result = _mvsApplicants.ValidateStatusChangeToAccepted(_applicantsFixture.dbmApplicantError);
      string errorsMessages = string.Join("-", _validators.errors.Select(x => x.error).ToList());
      Assert.Contains(MSError.No_Change_Status.error, errorsMessages);
      Assert.Null(result);
    }
  }
}
