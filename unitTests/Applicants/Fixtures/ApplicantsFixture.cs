﻿using AutoMapper;
using library.ControllerApplicants.RestModel;
using library.DataBase.Applicants;
using library.DataBase.Applicants.Models;
using library.General;
using library.General.Enums;
using Microsoft.Extensions.Options;
using Moq;

namespace unitTests.Applicants.Fixtures
{
  public class ApplicantsFixture
  {
    #region Properties
    public IOptions<AppSettings> AppSettings;
    public IMapper Mapper;

    public long? idSuccess = 1;
    public long? idError = 2;
    public DBMApplicants dbmApplicantSuccess;
    public DBMApplicants dbmApplicantError;
    public RMBodyApplication rmBodyApplicationsSuccess;
    public RMBodyApplication rmBodyApplicationsError;
    public Mock<IDBCApplicants> IDBApplicantsMock { get; }
    #endregion  Properties

    #region Constructor
    public ApplicantsFixture()
    {
      #region AppSettings
      AppSettings appSettings = new AppSettings();
      AppSettings = Options.Create(appSettings);
      #endregion AppSettings

      #region Mapper
      var config = new MapperConfiguration(cfg =>
      {
        cfg.AddProfile(new MapperConfigurationProfile());
      });

      Mapper = config.CreateMapper();
      #endregion

      #region Models
      rmBodyApplicationsSuccess = new RMBodyApplication() {
        firstName = "Juan",
        lastName = "Hernandez",
        identifier = "123XA",
        age = 31,
        magicalAffinity = MagicalAffinity.Tierra.ToString(),
      };

      rmBodyApplicationsError = new RMBodyApplication()
      {
        firstName = "Juan.12345678912345678",
        lastName = "Hernandez.12345678912345678?",
        identifier = "123XA?*ASDFGHJ",
        age = 1,
        magicalAffinity = "No exists",
      };


      dbmApplicantSuccess = new DBMApplicants()
      {
        id = idSuccess,
        firstName = "Juan",
        lastName = "Hernandez",
        identifier = "123XA",
        age = 31,
        grimorio = Grimorio.BuenaFortuna.ToString(),
        status = Status.Pendiente.ToString(),
        magicalAffinity = MagicalAffinity.Tierra.ToString(),
        createDate = DateTime.Now
      };

      dbmApplicantError = new DBMApplicants()
      {
        id = idSuccess,
        firstName = "",
        lastName = "",
        identifier = "",
        age = 0,
        grimorio = Grimorio.BuenaFortuna.ToString(),
        status = Status.Pendiente.ToString(),
        magicalAffinity = "",
        createDate = DateTime.Now
      };

      #endregion Models

      #region GxDBDBMSSqlMock
      IDBApplicantsMock = new Mock<IDBCApplicants>();
      IDBApplicantsMock.Setup(x => x.GetApplicantsAsync(idSuccess, null, null)).ReturnsAsync(new List<DBMApplicants>() { this.dbmApplicantSuccess });
      IDBApplicantsMock.Setup(x => x.GetApplicantsAsync(idError, null, null)).ReturnsAsync(new List<DBMApplicants>());

      #endregion GxDBDBMSSqlMock
    }
    #endregion Constructor

  }
}
