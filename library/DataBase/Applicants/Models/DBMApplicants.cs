﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace library.DataBase.Applicants.Models
{
  public class DBMApplicants
  {
    #region Atributos
    public long? id { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string identifier { get; set; }
    public int? age { get; set; }
    public int? grimorioId { get; set; }
    public string grimorio { get; set; }
    public int? statusId { get; set; }
    public string status { get; set; }
    public int? magicalAffinityId { get; set; }
    public string magicalAffinity { get; set; }
    public DateTime? createDate { get; set; }
    public DateTime? acceptanceDate { get; set; }
    #endregion Atributos

    #region Constructor
    public DBMApplicants() : this(null, null, null, null, null, null, null, null, null, null,null, null, null) { }
    public DBMApplicants(long? id, string firstName, string lastName, string identifier, int? age, 
      int? grimorioId, string grimorio, int? statusId, string status, int? magicalAffinityId, string magicalAffinity, DateTime? createDate, DateTime? acceptanceDate)
    {
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.identifier = identifier;
      this.age = age;
      this.grimorioId = grimorioId;
      this.grimorio = grimorio;
      this.statusId = statusId;
      this.status = status;
      this.magicalAffinityId = magicalAffinityId;
      this.magicalAffinity = magicalAffinity;
      this.createDate = createDate;
      this.acceptanceDate = acceptanceDate;
    }
    #endregion Constructor

    public List<SqlParameter> getSQLParameters
    {
      get {
        return new List<SqlParameter>()
        {
            new SqlParameter("id", this.id),
            new SqlParameter("firstName", this.firstName),
            new SqlParameter("lastName", this.lastName),
            new SqlParameter("identifier", this.identifier),
            new SqlParameter("age", this.age),
            new SqlParameter("grimorioId", this.grimorioId),
            new SqlParameter("statusId", this.statusId),
            new SqlParameter("magicalAffinityId", this.magicalAffinityId),
            new SqlParameter("createDate", this.createDate),
            new SqlParameter("acceptanceDate", this.acceptanceDate),
        };
      }
    }
  }
}
