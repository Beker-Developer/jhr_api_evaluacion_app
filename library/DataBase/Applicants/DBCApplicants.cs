﻿using library.DataBase.Applicants.Models;
using library.General;
using library.General.Enums;
using MSSQL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace library.DataBase.Applicants
{
  public interface IDBCApplicants
  {
    Task<List<DBMApplicants>> GetApplicantsAsync(long? id, string identifier, string grimorio);
    Task<DBMApplicants> InsertApplicantsAsync(DBMApplicants dbmApplicants);
    Task DeleteApplicantsAsync(long? id);
  }
  public class DBCApplicants : IDBCApplicants
  {
    public string spName = "sp_Applicants";
    private readonly IMSSQLDB _msSQLDB;
    public DBCApplicants(IMSSQLDB gxDBMSSql)
    {
      _msSQLDB = gxDBMSSql;
    }

    #region Gets
    public async Task<List<DBMApplicants>> GetApplicantsAsync(long? id, string identifier, string grimorio)
    {
      Grimorio? grimorioEnum = EnumUtils.ParseEnumFromString<Grimorio>(grimorio);
      List<SqlParameter> SqlParameter = new List<SqlParameter>()
      {
        new SqlParameter("@Action", "1")
      };

      if (id != null) SqlParameter.Add(new SqlParameter("@id", id));
      if (identifier != null) SqlParameter.Add(new SqlParameter("@identifier", identifier));
      if(grimorioEnum != null) SqlParameter.Add(new SqlParameter("@grimorioId", (int)grimorioEnum));

      List<DBMApplicants> dbmApplicants = new List<DBMApplicants>();
      DataTable dt_response = await _msSQLDB.GetData(spName, SqlParameter);
      foreach (DataRow dr in dt_response.Rows)
      {
        dbmApplicants.Add(new DBMApplicants(
          long.Parse(dr["id"].ToString()),
          dr["firstName"].ToString(),
          dr["lastName"].ToString(),
          dr["identifier"].ToString(),
          int.TryParse(dr["age"].ToString(), out int age) ? age : default(int?),
          int.TryParse(dr["grimorioId"].ToString(), out int grimorioId) ? grimorioId : default(int?),
          dr["grimorio"].ToString(),
          int.TryParse(dr["statusId"].ToString(), out int statusId) ? statusId : default(int?),
          dr["status"].ToString(),
          int.TryParse(dr["magicalAffinityId"].ToString(), out int magicalAffinityId) ? magicalAffinityId : default(int?),
          dr["magicalAffinity"].ToString(),
          DateTime.TryParse(dr["createDate"].ToString(), out DateTime createDate) ? createDate : default(DateTime?),
          DateTime.TryParse(dr["acceptanceDate"].ToString(), out DateTime acceptanceDate) ? acceptanceDate : default(DateTime?)
       ));
      }

      return dbmApplicants;
    }
    #endregion Gets

    #region Insert
    public async Task<DBMApplicants> InsertApplicantsAsync(DBMApplicants dbmApplicants)
    {
      List<SqlParameter> SqlParameter = dbmApplicants.getSQLParameters;
      SqlParameter.Add(new SqlParameter("@Action", 2));
      object id = await _msSQLDB.ExecuteScalar(spName, SqlParameter);
      dbmApplicants.id = long.Parse(id.ToString());
      return dbmApplicants;
    }

    #endregion

    #region Update
    public async Task<DBMApplicants> UpdateApplicantsAsync(DBMApplicants dbmApplicants)
    {
      List<SqlParameter> SqlParameter = dbmApplicants.getSQLParameters;
      SqlParameter.Add(new SqlParameter("@Action", 3));
      await _msSQLDB.ExecuteScalar(spName, SqlParameter);
      return dbmApplicants;
    }

    #endregion Update

    #region Delete
    public async Task DeleteApplicantsAsync(long? id)
    {
      List<SqlParameter> SqlParameter = new List<SqlParameter>();
      SqlParameter.Add(new SqlParameter("@Action", 4));
      SqlParameter.Add(new SqlParameter("@id", id));
      await _msSQLDB.ExecuteNonQueryAsync(spName, SqlParameter);
    }

    #endregion Delete

  }
}
