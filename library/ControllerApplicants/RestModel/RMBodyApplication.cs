﻿namespace library.ControllerApplicants.RestModel
{
  public class RMBodyApplication
  {
    #region Atributos
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string identifier { get; set; }
    public int? age { get; set; }
    public string magicalAffinity { get; set; }

    #endregion Atributos

    #region Constructor
    public RMBodyApplication() : this(null, null, null, null, null) { }
    public RMBodyApplication(string firstName, string lastName, string identifier, int? age,
      string magicalAffinity)
    {
      this.firstName = firstName;
      this.lastName = lastName;
      this.identifier = identifier;
      this.age = age;
      this.magicalAffinity = magicalAffinity;
    }
    #endregion Constructor
  }
}
