﻿namespace library.ControllerApplicants.RestModel
{
  public class RMApplicantStatus
  {
    public string status { get; set; }
    public RMApplicantStatus() : this(null) { }
    public RMApplicantStatus(string status)
    {
      this.status = status;
    }
  }
}
