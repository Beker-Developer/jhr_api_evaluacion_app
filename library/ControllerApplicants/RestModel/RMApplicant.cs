﻿using System;

namespace library.ControllerApplicants.RestModel
{
  public class RMApplicant : RMBodyApplication
  {
    #region Atributos
    public long? id { get; set; }
    public string grimorio { get; set; }
    public string frontPage { get; set; }
    public string status { get; set; }
    public DateTime? createDate { get; set; }
    #endregion Atributos

    #region Constructor
    public RMApplicant() : this(null, null, null, null, null, null, null, null, null, null) { }
    public RMApplicant(long? id, string firstName, string lastName, string identifier, int? age,
      string grimorio, string status, string magicalAffinity, DateTime? createDate, DateTime? acceptanceDate)
    {
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.identifier = identifier;
      this.age = age;
      this.grimorio = grimorio;
      this.status = status;
      this.magicalAffinity = magicalAffinity;
      this.createDate = createDate;
    }
    #endregion Constructor
  }
}
