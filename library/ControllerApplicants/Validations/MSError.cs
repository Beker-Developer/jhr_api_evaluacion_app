﻿using library.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace library.ControllerApplicants.Validations
{
  public static class MSError
  {
    public static Error No_Exists_Grimorio
    { 
      get 
      { 
        return new Error("grimorio", "El valor del griomo no corresponde con el catalogo, los valores permitidos son: Sinceridad, Esperanza, Amor, Buena Fortuna o Desesperacion"); 
      } 
    }

    public static Error No_Exists_Magical_Affinity
    {
      get
      {
        return new Error("magicalAffinity", "El valor de la afinidad magica no corresponde con el catalogo, los valores permitidos son: Oscuridad, Luz, Fuego, Agua, Viento o Tierra");
      }
    }

    public static Error No_Exists_Status
    {
      get
      {
        return new Error("status", "El valor del estatus no corresponde con el catalogo, los valores permitidos son: Pendiente, Revisión, Aceptado o Rechazado");
      }
    }

    public static Error Exists_Identifier { 
      get 
      { 
        return new Error("identifier", "El identificador ya se encuentra registrado"); 
      } 
    }

    public static Error No_Exists_Identifier
    {
      get
      {
        return new Error("id", "El aspirante no se encuentra registrado");
      }
    }

    public static Error Name_Exceed_Character_Limit
    {
      get
      {
        return new Error("name", "El nombre excede el maximo de 20 caracteres");
      }
    }

    public static Error Name_Contains_Only_Letter
    {
      get
      {
        return new Error("name", "El nombre solo puede contener letras");
      }
    }

    public static Error Last_Name_Exceed_Character_Limit
    {
      get
      {
        return new Error("lastName", "El apellido excede el maximo de 20 caracteres");
      }
    }

    public static Error Last_Name_Contains_Only_Letter
    {
      get
      {
        return new Error("lastName", "El apellido solo puede contener letras");
      }
    }

    public static Error Identifier_Exceed_Character_Limit
    {
      get
      {
        return new Error("identifier", "El identificador excede el maximo de 10 caracteres");
      }
    }

    public static Error Identifier_Contains_Only_Letter_And_Numbers
    {
      get
      {
        return new Error("identifier", "El identificador solo puede contener numeros y letras");
      }
    }

    public static Error Age_Exceed_Character_Limit
    {
      get
      {
        return new Error("Ag", "La edad debe contener 2 digitos");
      }
    }

    public static Error No_Change_Status
    {
      get
      {
        return new Error("firstName, lastName, age, identifier, magicalAffinity", "Los campos nombres, apellidos, edad, identificador y afinidad magica son requeridos para poder aceptar al aspirante");
      }
    }
  }
}
