﻿using library.DataBase.Applicants;
using library.DataBase.Applicants.Models;
using library.General;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library.ControllerApplicants.Validations
{
  public class MVDApplicants
  {
    IDBCApplicants _dbcApplicants;
    private readonly IValidators _validators;

    public MVDApplicants(IDBCApplicants dbcApplicants, IValidators validators)
    {
      _dbcApplicants = dbcApplicants;
     _validators = validators;
    }

    public async Task<DBMApplicants> ValidateDBMApplicantsExistAsync(long? id, string identifier)
    {
      List<DBMApplicants> dbmApplicants = await _dbcApplicants.GetApplicantsAsync(id, identifier,  null);

      if (dbmApplicants.Count > 0)
        _validators.Add(MSError.Exists_Identifier);

      return _validators.ContainErrors() ? null : dbmApplicants.FirstOrDefault();
    }

    public async Task<DBMApplicants> ValidateExistsApplicantsAsync(long? id)
    {
      List<DBMApplicants> dbmApplicants = await _dbcApplicants.GetApplicantsAsync(id, null, null);

      if (dbmApplicants.Count == 0)
        _validators.Add(MSError.No_Exists_Identifier);

      return _validators.ContainErrors() ? null : dbmApplicants.FirstOrDefault();
    }
  }
}
