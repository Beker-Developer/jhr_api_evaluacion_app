﻿using library.ControllerApplicants.RestModel;
using library.DataBase.Applicants.Models;
using library.General;
using library.General.Enums;
using System;

namespace library.ControllerApplicants.Validations
{
  public class MVSApplicants
  {
    private readonly IValidators _validators;
    private IARegex _iaRegex;

    public MVSApplicants(IValidators validators)
    {
      _validators = validators;
      _iaRegex = new IARegex();
    }

    public DBMApplicants ValidateApplicants(long? id, RMBodyApplication rmApplicants, bool isNew, DBMApplicants dbmCurrentApplicants = null)
    {
      MagicalAffinity? magicalAffinity;
      Status? status = Status.Pendiente;

      DBMApplicants dbmApplicants = new DBMApplicants();
      dbmApplicants.id = id;
      dbmApplicants.firstName = rmApplicants.firstName;
      dbmApplicants.lastName = rmApplicants.lastName;
      dbmApplicants.identifier = rmApplicants.identifier;
      dbmApplicants.age = rmApplicants.age;

      if (dbmCurrentApplicants != null)
      {
        status = EnumUtils.ParseEnumFromString<Status>(dbmCurrentApplicants.status);
        dbmApplicants.identifier = dbmCurrentApplicants.identifier;
        dbmApplicants.acceptanceDate = dbmCurrentApplicants.acceptanceDate;
        dbmApplicants.grimorioId = dbmCurrentApplicants.grimorioId;
        dbmApplicants.grimorio = dbmCurrentApplicants.grimorio;
        dbmApplicants.createDate = dbmCurrentApplicants.createDate;

      }

      if (dbmApplicants.firstName != null)
      {
        if (dbmApplicants.firstName.Length > 20 && dbmApplicants.firstName.Length > 0)
          _validators.Add(MSError.Name_Exceed_Character_Limit);

        if (!_iaRegex.ContainsOnlyLetter.IsMatch(dbmApplicants.firstName) && dbmApplicants.firstName.Length > 0)
          _validators.Add(MSError.Name_Contains_Only_Letter);
      }

      if (dbmApplicants.lastName != null) {
        if (dbmApplicants.lastName.Length > 20 && dbmApplicants.lastName.Length > 0)
          _validators.Add(MSError.Last_Name_Exceed_Character_Limit);

        if (!_iaRegex.ContainsOnlyLetter.IsMatch(dbmApplicants.lastName) && dbmApplicants.lastName.Length > 0)
          _validators.Add(MSError.Last_Name_Contains_Only_Letter);
      }

      if (dbmApplicants.identifier != null)
      {
        if (dbmApplicants.identifier.Length > 10 && dbmApplicants.identifier.Length > 0)
          _validators.Add(MSError.Identifier_Exceed_Character_Limit);

        if (!_iaRegex.ContainsLetterAndNumber.IsMatch(dbmApplicants.identifier) && dbmApplicants.identifier.Length > 0)
          _validators.Add(MSError.Identifier_Contains_Only_Letter_And_Numbers);
      }

      if (dbmApplicants.age != null)
        if (dbmApplicants.age < 10 || dbmApplicants.age > 100)
          _validators.Add(MSError.Age_Exceed_Character_Limit);

      if (rmApplicants.magicalAffinity != null && rmApplicants.magicalAffinity.Length > 0)
      {
        magicalAffinity = EnumUtils.ParseEnumFromString<MagicalAffinity>(rmApplicants.magicalAffinity);
        if (magicalAffinity != null)
        {
          dbmApplicants.magicalAffinityId = (int)magicalAffinity;
          dbmApplicants.magicalAffinity = rmApplicants.magicalAffinity;
        }
        else
          _validators.Add(MSError.No_Exists_Magical_Affinity);
      }
      status = String.IsNullOrEmpty(dbmApplicants.firstName) ||
        String.IsNullOrEmpty(dbmApplicants.magicalAffinity) ||
          String.IsNullOrEmpty(dbmApplicants.lastName) ||
          (isNew ? String.IsNullOrEmpty(dbmApplicants.identifier) : false) ||
         (dbmApplicants.age ?? 0) == 0 ? Status.Rechazado : status;

      if(status == Status.Rechazado) {
        dbmApplicants.grimorio = null;
        dbmApplicants.grimorioId = null;
      }
      

      dbmApplicants.statusId = (int)status;
      dbmApplicants.status = status.ToString();
      dbmApplicants.createDate = dbmApplicants.createDate ?? DateTime.Now;

      return _validators.ContainErrors() ? null : dbmApplicants;
    }

    public Status? ValidateStatus(string status) {
      Status? statusEnum = EnumUtils.ParseEnumFromString<Status>(status);
      if(statusEnum == null)
        _validators.Add(MSError.No_Exists_Status);
      return statusEnum;
    }

    public Grimorio? ValidateStatusChangeToAccepted(DBMApplicants dbmCurrentApplicants)
    {
      Grimorio? grimorio = null;
      Status? status = String.IsNullOrEmpty(dbmCurrentApplicants.firstName) ||
        String.IsNullOrEmpty(dbmCurrentApplicants.magicalAffinity) ||
          String.IsNullOrEmpty(dbmCurrentApplicants.lastName) ||
           String.IsNullOrEmpty(dbmCurrentApplicants.identifier) ||
         dbmCurrentApplicants.age == 0? Status.Rechazado: Status.Aceptado;

      if (status == Status.Rechazado)
        _validators.Add(MSError.No_Change_Status);
      else
      {
        int? grimorioId = new Random().Next(1, 5);
        grimorio = (Grimorio)grimorioId;
      }

      return grimorio;
    }
  }
}
