﻿using AutoMapper;
using library.ControllerApplicants.RestModel;
using library.ControllerApplicants.Validations;
using library.DataBase.Applicants;
using library.DataBase.Applicants.Models;
using library.General;
using library.General.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MSSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library.ControllerApplicants
{
  public class MCApplicants
  {
    private readonly Validators _validator;
    private readonly MVSApplicants _mvsApplicants;
    private readonly MVDApplicants _mvdApplicants;

    private readonly DBCApplicants _dbcApplicants;
    IMapper _mapper;

    #region Constructor
    public MCApplicants(IOptions<AppSettings> appSettings, IMapper mapper, IMSSQLDB msSQLDB)
    {
      _mapper = mapper;
      _validator = new Validators();
      _dbcApplicants = new DBCApplicants(msSQLDB);
      _mvsApplicants = new MVSApplicants(_validator);
      _mvdApplicants = new MVDApplicants(_dbcApplicants, _validator);
    }
    #endregion Constructor

    #region GET
    public async Task<ActionResult> Get(long? id, string identifier, string grimorio)
    {
      List<DBMApplicants> dbmApplicants = await _dbcApplicants.GetApplicantsAsync(id, identifier, grimorio);
      List<RMApplicant> rmAccountingFirms = _mapper.Map<List<RMApplicant>>(dbmApplicants);
      if (id != null || identifier != null)
        if (rmAccountingFirms.Count == 0)
          return new NotFoundResult();
        else
          return new OkObjectResult(rmAccountingFirms.FirstOrDefault());
      return new OkObjectResult(rmAccountingFirms);
    }
    #endregion GET

    #region POST
    public async Task<ActionResult> Post(RMBodyApplication rmApplicants)
    {
      #region Structure validator
      DBMApplicants dbmApplicants = _mvsApplicants.ValidateApplicants(null,rmApplicants, true);
      if (_validator.ContainErrors())
        goto FinishBadRequest;
      #endregion

      #region Data validator
      if (dbmApplicants.identifier != null && dbmApplicants.identifier.Length > 0)
      {
        await _mvdApplicants.ValidateDBMApplicantsExistAsync(dbmApplicants.id, dbmApplicants.identifier);
        if (_validator.ContainErrors())
          goto FinishBadRequest;
      }
      #endregion

      #region Save
      try
      {
        dbmApplicants = await _dbcApplicants.InsertApplicantsAsync(dbmApplicants);
      }
      catch (Exception ex)
      {
        _validator.Add("database", ex.Message);
      }


      #endregion
      return new OkObjectResult(_mapper.Map<RMApplicant>(dbmApplicants)) { StatusCode = StatusCodes.Status201Created };

    FinishBadRequest:
      return new BadRequestObjectResult(new { errors = _validator.GetErrors() });

    }
    #endregion POST

    #region PUT
    public async Task<ActionResult> Put(long? id, RMBodyApplication rmApplicants)
    {
      #region Structure validator
      DBMApplicants dbmApplicantsSave = await _mvdApplicants.ValidateExistsApplicantsAsync(id);
      if (_validator.ContainErrors())
        return new NotFoundObjectResult(new { errors = _validator.GetErrors() });
      #endregion

      #region Data validator
      DBMApplicants dbmApplicantsNew = _mvsApplicants.ValidateApplicants(id, rmApplicants, false, dbmApplicantsSave);
      if (_validator.ContainErrors())
        goto FinishBadRequest;
      #endregion  Data validator

      #region Save
      try
      {
        dbmApplicantsNew = await _dbcApplicants.UpdateApplicantsAsync(dbmApplicantsNew);
      }
      catch (Exception ex)
      {
        _validator.Add("database", ex.Message);
      }

      #endregion
      return new ObjectResult(_mapper.Map<RMApplicant>(dbmApplicantsNew));

    FinishBadRequest:
      return new BadRequestObjectResult(new { errors = _validator.GetErrors() });

    }

    public async Task<ActionResult> PutStatus(long? id, RMApplicantStatus rmStatus)
    {
      #region Structure validator
      Status? statusEnum = _mvsApplicants.ValidateStatus(rmStatus.status);
      if (_validator.ContainErrors())
        goto FinishBadRequest;
      #endregion

      #region Data validator
      DBMApplicants dbmApplicantsSave = await _mvdApplicants.ValidateExistsApplicantsAsync(id);
      if (_validator.ContainErrors())
        goto FinishBadRequest;

      dbmApplicantsSave.statusId = (int)statusEnum;
      dbmApplicantsSave.status = statusEnum.ToString();
      #endregion  Data validator

      #region Save
      try
      {
        if (statusEnum == Status.Aceptado)
        {
          Grimorio? grimorio = _mvsApplicants.ValidateStatusChangeToAccepted(dbmApplicantsSave);
          if (_validator.ContainErrors())
            goto FinishBadRequest;
          dbmApplicantsSave.grimorio = grimorio.ToString();
          dbmApplicantsSave.grimorioId = (int)grimorio;
        }
        else
        {
          dbmApplicantsSave.grimorio = null;
          dbmApplicantsSave.grimorioId = null;
        }

        dbmApplicantsSave = await _dbcApplicants.UpdateApplicantsAsync(dbmApplicantsSave);
      }
      catch (Exception ex)
      {
        _validator.Add("database", ex.Message);
      }
      #endregion
      return new OkObjectResult(_mapper.Map<RMApplicant>(dbmApplicantsSave));

    FinishBadRequest:
      return new BadRequestObjectResult(new { errors = _validator.GetErrors() });

    }
    #endregion PUT

    #region Delete
    public async Task<ActionResult> Delete(long? id)
    {
      #region Exists validator
      DBMApplicants dbmApplicantsSave = await _mvdApplicants.ValidateExistsApplicantsAsync(id);
      if (_validator.ContainErrors())
        return new NotFoundObjectResult(new { errors = _validator.GetErrors() });
      #endregion

      #region Save
      try
      {
        await _dbcApplicants.DeleteApplicantsAsync(id);
        return new NoContentResult();
      }
      catch (Exception ex)
      {
        _validator.Add("database", ex.Message);
      }

      #endregion
      return new BadRequestObjectResult(new { errors = _validator.GetErrors() });

    }
    #endregion Delete

  }
}
