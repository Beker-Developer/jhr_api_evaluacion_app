﻿using System;
using System.Text.RegularExpressions;

namespace library.General
{
  public static class EnumUtils
  {
    public static T? ParseEnumFromString<T>(string value) where T : struct, IConvertible
    {
      T? result = null;
      if (!typeof(T).IsEnum) throw new ArgumentException("T must be an enum type");
      if (string.IsNullOrEmpty(value)) goto NoExist;

      foreach (T item in Enum.GetValues(typeof(T)))
      {
        if (item.ToString().ToLower().Equals(Regex.Replace(value.Trim().ToLower(), @"\s", ""))) return item;
      }
      NoExist:
      return result;
    }

  }
}
