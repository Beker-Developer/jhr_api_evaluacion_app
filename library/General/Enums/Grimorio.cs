﻿namespace library.General.Enums
{
  public enum Grimorio
  {
    Sinceridad = 1,
    Esperanza = 2,
    Amor = 3,
    BuenaFortuna = 4,
    Desesperacion = 5
  }
}
