﻿namespace library.General.Enums
{
  public enum MagicalAffinity
  {
    Oscuridad = 1,
    Luz = 2,
    Fuego = 3,
    Agua = 4,
    Viento = 5,
    Tierra = 6
  }
}
