﻿namespace library.General.Enums
{
  public enum Status
  {
    Pendiente = 1,
    Revisión = 2,
    Aceptado = 3,
    Rechazado = 4,
  }
}
