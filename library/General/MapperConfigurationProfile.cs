﻿using AutoMapper;
using library.ControllerApplicants.RestModel;
using library.DataBase.Applicants.Models;
using library.General.Enums;

namespace library.General
{
  public class MapperConfigurationProfile : Profile
  {
    public MapperConfigurationProfile()
  : this("MyProfile")
    {
    }
    protected MapperConfigurationProfile(string profileName)
   : base(profileName)
    {
      CreateMap<DBMApplicants, RMApplicant>().ForMember(destination => destination.frontPage, opts =>
      {
        opts.PreCondition(source => (source.grimorioId != null));
        opts.MapFrom(source => GetPageFrontPage(source.grimorioId));
      });
      CreateMap<RMApplicant, DBMApplicants>();
    }

    private string GetPageFrontPage(int? grimorioId) {
      switch (grimorioId)
      {
        case (int)Grimorio.Sinceridad:
          return "Trébol de 1 hoja";
        case (int)Grimorio.Esperanza:
          return "Trébol de 2 hoja";
        case (int)Grimorio.Amor:
          return "Trébol de 3 hoja";
        case (int)Grimorio.BuenaFortuna:
          return "Trébol de 4 hoja";
        case (int)Grimorio.Desesperacion:
          return "Trébol de 5 hoja";
        default:
          return null;
      }
    }
  }
}
