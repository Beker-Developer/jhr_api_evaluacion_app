﻿using System.Text.RegularExpressions;

namespace library.General
{
  public class IARegex
  {
    public Regex ContainsOnlyLetter = new Regex(@"^[a-zA-Z]+$");
    public Regex ContainsLetterAndNumber = new Regex(@"^[a-zA-Z0-9]+$");
  }
}
