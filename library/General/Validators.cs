﻿using System.Collections.Generic;

namespace library.General
{
  public interface IValidators
  {
    void Add(string field, string error);
    void Add(Error error);
    void ClearErrors();
    bool ContainErrors();
    List<Error> GetErrors();
  }
  public class Validators: IValidators
  {
    public List<Error> errors { get; set; }
    public Validators()
    {
      this.errors = new List<Error>();
    }
    public Validators(ref List<Error> errors)
    {
      this.errors = errors;
    }

   
    public void Add(string field, string error)
    {
      errors.Add(new Error(field, error));
    }
    public void Add(Error error)
    {
      errors.Add(error);
    }
    public void ClearErrors() {
      errors = new List<Error>();
    }
    public bool ContainErrors() => errors.Count > 0;
    public List<Error> GetErrors()
    {
      return errors;
    }
  }

  public class Error
  {
    public string field { get; set; }
    public string error { get; set; }
    public Error() 
      :this(null,null)
    { 
    }
    public Error(string field, string error)
    {
      this.field = field;
      this.error = error;
    }
  }
}
