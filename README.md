# jhr_api_evaluacion_app

Autor: Juan Antonio Hernandez Rosales

## Descripción

Examen práctico de ingreso IA Interactive, para evaluar mis capacidades técnicas, documentada en Swagger y PostMan

## Instalación y depuración

A continuación se presentan una lista de pasos para poder correr el proyecto.

##### Requisitos

#

Se requiere tener instalado

- .NET 7 ([Link](https://dotnet.microsoft.com/en-us/download/dotnet/7.0))
- Visual Studio 2022 ([Link](https://visualstudio.microsoft.com/es/thank-you-downloading-visual-studio/?sku=Community&channel=Release&version=VS2022&source=VSLandingPage&cid=2030&passive=false))
- Microsoft SQL Server ([Link](https://go.microsoft.com/fwlink/?linkid=866662))
- POSTMAN (opcional) ([Link](https://www.postman.com/downloads/?utm_source=postman-home))

##### Crear la base de datos.

#

Descargar el esquema de base de datos y ejecuté el script en su servidor **Microsoft SQL Server** local ([Download Database Schema](https://gitlab.com/Beker-Developer/jhr_api_evaluacion_app/-/blob/main/MSSQL/IA%20Interactivet.sql "download"))

##### Clonar proyecto

##

Clone el proyecto en la carpeta deseada con el comando:

```
git clone https://gitlab.com/Beker-Developer/jhr_api_evaluacion_app.git
```

##### Abrir proyecto

#

Abra **Visual Studio 2022** y seleccione **Abrir un proyecto o una solución**, busque el archivo **jhr_api_evaluacion_app.sln** en la carpeta donde se clonó el proyecto y selecciónelo.

##### Configuración del proyecto

#

Para que el proyecto pueda consumir la información de la base de datos es necesario **cambiar la cadena de conexión**, para ello en su Visual Studio localicé el archivo **appsettings.json** que se encuentra dentro de la raíz del proyecto **jhr_api_evaluacion_app** y modifique el nombre del servidor, usuario y password por los que correspondan con su servidor de base de datos MSSQL local.

Ejemplo de cadena:

```
Data Source={Server};Connection Timeout=0;Initial Catalog=IA_Interactive;User ID={User}; Password={Pass}
```

Si lo necesita puede consultar las siguientes ligas para saber como crear un usuario en su servidor MSSQL

- Instalar MSSQL ([Link](https://jotelulu.com/soporte/tutoriales/como-instalar-un-sql-server-2019-en-mi-servidor-windows/))
- Crear Usuario ([Link](https://www.guru99.com/sql-server-create-user.html))

##### Correr y probar proyecto

#

Localice el botón verde con icono de play para empezar a correr el proyecto.

Puede probar el API desde la ventana de swagger que se abre directamente o si lo prefiere, puede descargar la colección desde el portal de POSTMANT

- ([Link de la Colección](https://www.postman.com/juandevelop/workspace/ia-iteractive/request/3417939-0fd00a98-4e93-4bfd-a67c-47f0d57117a6))
- ([Link de variables](https://www.postman.com/juandevelop/workspace/ia-iteractive/environment/3417939-40ac42fc-7153-4dbd-bb51-0ebfcc6de8a3))
- ([Publicacion con ejemplos](https://documenter.getpostman.com/view/3417939/2s93CPrYR1))

##### Pruebas automatizadas

Se agregan pruebas initarias en el proyecto **unitTests**
Se agregan pruebas de integración en el proyecto **integrationTests**

#

#

#
